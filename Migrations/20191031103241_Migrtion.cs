﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace WebApplication1.Migrations
{
    public partial class Migrtion : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "MarkerCategoryId",
                table: "Markers",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "MarkerStateId",
                table: "Markers",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "MarkerCategory",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Title = table.Column<string>(nullable: false),
                    IconPath = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MarkerCategory", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MarkerState",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Code = table.Column<string>(nullable: false),
                    Color = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MarkerState", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Markers_MarkerCategoryId",
                table: "Markers",
                column: "MarkerCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Markers_MarkerStateId",
                table: "Markers",
                column: "MarkerStateId");

            migrationBuilder.AddForeignKey(
                name: "FK_Markers_MarkerCategory_MarkerCategoryId",
                table: "Markers",
                column: "MarkerCategoryId",
                principalTable: "MarkerCategory",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Markers_MarkerState_MarkerStateId",
                table: "Markers",
                column: "MarkerStateId",
                principalTable: "MarkerState",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Markers_MarkerCategory_MarkerCategoryId",
                table: "Markers");


            migrationBuilder.DropForeignKey(
                name: "FK_Markers_MarkerState_MarkerStateId",
                table: "Markers");

            migrationBuilder.DropTable(
                name: "MarkerCategory");

            migrationBuilder.DropTable(
                name: "MarkerState");

            migrationBuilder.DropIndex(
                name: "IX_Markers_MarkerCategoryId",
                table: "Markers");

            migrationBuilder.DropIndex(
                name: "IX_Markers_MarkerStateId",
                table: "Markers");

            migrationBuilder.DropColumn(
                name: "MarkerCategoryId",
                table: "Markers");

            migrationBuilder.DropColumn(
                name: "MarkerStateId",
                table: "Markers");
        }
    }
}
