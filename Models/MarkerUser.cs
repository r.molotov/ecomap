﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class MarkerUser
    {
        public int MarkerId { get; set; }
        public virtual Marker marker { get; set; }


        public int UserId { get; set; }
        public virtual User user { get; set; }
    }
}
