﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace WebApplication1.Models
{
    public class EcoContext : DbContext
    {

        public DbSet<User> Users { get; set; }
        public DbSet<Marker> Markers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MarkerUser>()
                .HasKey(mu => new { mu.MarkerId, mu.UserId });

            modelBuilder.Entity<MarkerUser>()
                .HasOne(mu => mu.marker)
                .WithMany(m => m.MarkerUsers)
                .HasForeignKey(mu => mu.MarkerId);

            modelBuilder.Entity<MarkerUser>()
                .HasOne(mu => mu.user)
                .WithMany(u => u.MarkerUsers)
                .HasForeignKey(mu => mu.UserId);
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder
                .UseLazyLoadingProxies()
                .UseNpgsql("Host=localhost;Database=test;Username=postgres;Password=123");

    }
}
