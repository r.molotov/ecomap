﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class Marker
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public float Coords { get; set; }
        
        public int OwnerId { get; set; }
        public virtual User Owner { get; set; }

        public virtual List<MarkerUser> MarkerUsers { get; set; }

    }
}
